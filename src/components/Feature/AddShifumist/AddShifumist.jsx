import React from "react";
import { postShifumist } from "../../../logic/sendData";

class AddShifumist extends React.Component {
  constructor(props) {
    super(props);
    this.state = { nom: "", pierre: 0, feuille: 0, ciseau: 0 };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value =
      target.type === "number" ? parseInt(target.value) : target.value;
    const name = target.name;
    this.setState({ [name]: value });
  }
  checkform() {
    if (this.check100 && this.checknom) return true;
    else return false;
  }
  check100() {
    if (this.state.pierre + this.state.feuille + this.state.ciseau === 100)
      return true;
    else return false;
  }
  checknom() {
    if (this.state.nom !== "") return true;
    else return false;
  }

  handleSubmit(event) {
    postShifumist({
      nom: this.state.nom,
      pierre: this.state.pierre,
      feuille: this.state.feuille,
      ciseau: this.state.ciseau,
    });
  }

  render() {
    return (
      <>
        {this.check100() ? "" : "Attention, le total ne fait pas 100 !"}
        <br />
        {this.checknom() ? "" : "Attention, le nom est vide !"}
        <br />
        <label>
          Nom :
          <input
            name="nom"
            type="text"
            value={this.state.nom}
            onChange={this.handleInputChange}
          />
        </label>
        <br />
        <label>
          Pierre :
          <input
            name="pierre"
            type="number"
            value={this.state.pierre}
            onChange={this.handleInputChange}
          />
        </label>
        <br />
        <label>
          Feuille :
          <input
            name="feuille"
            type="number"
            value={this.state.feuille}
            onChange={this.handleInputChange}
          />
        </label>
        <br />
        <label>
          Ciseau :
          <input
            name="ciseau"
            type="number"
            value={this.state.ciseau}
            onChange={this.handleInputChange}
          />
        </label>
        <br />
        <button onClick={this.handleSubmit}>Envoyer</button>
      </>
    );
  }
}

export default AddShifumist;
