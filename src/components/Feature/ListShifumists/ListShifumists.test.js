import React from "react";
import { shallow } from "enzyme";
import ListShifumists from "./ListShifumists";

describe("ListShifumists", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ListShifumists />);
    expect(wrapper).toMatchSnapshot();
  });
});
