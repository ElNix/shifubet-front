import React from "react";
import { loadShifumists } from "../../../logic/getData";

class ListShifumists extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
    loadShifumists().then((data) => {
      console.table(data);
      this.setState({ users: data });
    });
  }

  render() {
    return (
      <>
        ListShifumists : <br />
        
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Nom</th>
                <th scope="col">Pierre</th>
                <th scope="col">Feuille</th>
                <th scope="col">Ciseau</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map((u) => (
                <tr key={u.nom}>
                  <th scope="row">{u.nom}</th>
                  <td>{u.pierre}</td>
                  <td>{u.feuille}</td>
                  <td>{u.ciseau}</td>
                </tr>
              ))}
            </tbody>
          </table>
        
      </>
    );
  }
}

export default ListShifumists;
