import React from "react";

import "bootstrap/dist/css/bootstrap.css";
import './App.css';

import ListShifumists from "./components/Feature/ListShifumists";
import AddShifumist from "./components/Feature/AddShifumist/AddShifumist";

function App() {
  
  return (
    <div className="App">
      <ListShifumists />
      <AddShifumist />
    </div>
  );
}

export default App;
