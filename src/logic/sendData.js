const APIbase = "http://localhost:8080/ShiFuSpring/";

export const postShifumist = (data) => {
    console.log(data);
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };
    fetch(APIbase, requestOptions)
    .then(response => console.log(response.json()));
}
